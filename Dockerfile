FROM haproxy:alpine

COPY ./entrypoint.sh /entrypoint.sh

CMD [ "/entrypoint.sh" ]
