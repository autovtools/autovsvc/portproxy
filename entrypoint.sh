#!/bin/sh
CONFIG_TEMPLATE="/etc/haproxy.cfg.tmpl"
CONFIG="/etc/haproxy.cfg"
if [ "${PROXY_ACL_CONFIG}" = "" ]; then
    if [ "${PROXY_ALLOWED_SRC}" != "" ]; then
        # Only allow localhost to connec to the proxy
        # (Workaround for no HOST_IP:HOST_PORT:CONTAINER_PORT in swarm mode)
PROXY_ACL_CONFIG="acl network_allowed src ${PROXY_ALLOWED_SRC}
        tcp-request connection reject if !network_allowed
"
    fi
fi
eval "echo \"$(cat "${CONFIG_TEMPLATE}")\"" > "${CONFIG}"
chmod 0400 "${CONFIG}"
# Invoke the official docker-entrypoint.sh with the generated config
/docker-entrypoint.sh haproxy -f "${CONFIG}"
