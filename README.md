# portproxy

A containerized `haproxy` sidecar used to accomplish tcp port fowarding into existing services without requiring `--cap-add NET_ADMIN` for iptables.

