#!/bin/bash
NAME="portproxy"
TAG=${1:-latest}
if [ "${REGISTRY}" = "" ]; then
    echo "REGISTRY environment variable must be set!"
    exit 1
fi
FULL="${REGISTRY}${NAME}:${TAG}"
docker tag "${NAME}" "${FULL}"
docker push "${FULL}"
